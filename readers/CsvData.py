import pandas as pd

class CsvData:
    def get_data(self, filename: str, columns: list):
        try:
            return self._normalize(pd.read_csv(filename, usecols=columns))
        except Exception as e:
            return e

    def _normalize(self, dataframe):
        for column in dataframe.columns:
            dataframe.loc[abs(dataframe[column]) > dataframe[column].mean() + 3*dataframe[column].std()] = dataframe[column].mean()
        return dataframe

    def splitter(self, filename, inputCols, outputCols):
        return self.get_data(filename, inputCols), self.get_data(filename, outputCols)
            