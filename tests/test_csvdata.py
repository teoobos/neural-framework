import unittest
import pandas
from readers.CsvData import CsvData

class TestCsvData(unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.reader = CsvData()

    def test_get_data(self):
        data = self.reader.get_data('data.csv', ['p', 'T'])
        self.assertEqual(type(data), pandas.core.frame.DataFrame)

        data = self.reader.get_data('notfound.csv', ['a', 'b'])
        self.assertEqual(type(data), FileNotFoundError)

        data = self.reader.get_data('data.csv', ['a', 'b'])
        self.assertEqual(type(data), ValueError)

    def test_split_data(self):
        X, Y = self.reader.splitter('data.csv', ['p'], ['T'])
        self.assertEqual(list(X.columns), ['p'])
        self.assertEqual(list(Y.columns), ['T'])

        X, Y = self.reader.splitter('notfound.csv', ['p'], ['T'])
        self.assertEqual(type(X), type(Y))

        X, Y = self.reader.splitter('data.csv', ['a'], ['b'])
        self.assertEqual(type(X), type(Y))
