import sys
import unittest
import models.functions.activations as function

class TestActivation(unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.max = 999999# max int value
        self.min = - self.max

    def test_inteval(self):
        values = [function.sigmoid(n) for n in range(self.min, self.max)]
        self.assertTrue(max(values) == 1.0)
        self.assertTrue(min(values) >= 0)

        d_values = [function.dsigmoid(n) for n in range(self.min, self.max)]
        self.assertTrue(max(d_values) == 0.25)
        self.assertTrue(min(d_values) == 0)

        values = [function.relu(n) for n in range(self.min, self.max)]
        #print(max(values))
        self.assertTrue(max(values) == 999998) # max int value
        self.assertTrue(min(values) == 0)

        d_values = [function.drelu(n) for n in range(self.min, self.max)]
        self.assertTrue(max(d_values) == 1)
        self.assertTrue(min(d_values) == 0)

        values = [function.tanh(n) for n in range(self.min, self.max)]
        self.assertTrue(max(values) == 1.0)
        self.assertTrue(min(values) == -1.0)
        
        d_values = [function.dtanh(n) for n in range(self.min, self.max)]
        self.assertTrue(max(d_values) == 1.0)
        self.assertTrue(min(d_values) == 0.0)

        values = [n for n in range(self.min, self.max)]
        self.assertEqual(max(function.softmax(values)), 0.0)

    
        