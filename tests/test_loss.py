import unittest
import numpy

import models.functions.loss as functions

class TestLoss(unittest.TestCase):
    def setUp(self):
        return super().setUp()

    def test_squared(self):
        x = numpy.array([1, 2, 3, 4, 5])
        y = numpy.array([1, 2, 3, 4, 5])

        result = functions.squared(x, y)

        self.assertEqual(sum(result), 0)

    def test_dsquared(self):
        x = numpy.array([1, 1, 1, 1, 1])
        y = numpy.array([0, 0, 0, 0, 0])

        result = functions.dsquared(x, y)
        
        self.assertEqual(sum(result), 2.0*5)

    def test_crossentropy(self):
        x = numpy.array([1, 1, 1, 1, 1])
        y = numpy.array([1, 1, 1, 1, 1])

        result = functions.cross_entropy(x, y)

        self.assertTrue(sum(result) > 0)
        
        