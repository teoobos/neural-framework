import numpy as np

from .functions.activations import dsigmoid, dtanh, sigmoid, softmax, tanh
#from .functions.utils import one_hot
from .functions.loss import cross_entropy
from .params.LSTMParams import LSTMParams


class LSTM:
    def __init__(self, input_dim, hidden_dim, output_dim, standard_deviation=0.01, learning_rate=1e-6, activation='sdg'):
        self.input = input_dim
        self.output = output_dim
        self.hidden = hidden_dim
        self.lr = learning_rate
        self.activation = activation
        #self.size = size
        self.params = LSTMParams(input_dim, hidden_dim, output_dim ,standard_deviation)
        self.cache = []
        self.state = []

    def forward(self, X):
        batch = X.shape[1]
        self.state.append([np.zeros([self.hidden, 1]),
                           np.zeros([self.hidden, 1])])
        for el in X:
            cprev, hprev = self.state[-1]
            el = el.reshape(3, 1)
            print(el.shape, hprev.shape)
            x = np.column_stack([el, hprev])
            
            #x = el.reshape(3,10)
            hf = sigmoid(x.dot(self.params.wf.value))
            hi = sigmoid(x.dot(self.params.wi.value))
            ho = sigmoid(x.dot(self.params.wo.value))
            hc = tanh(x.dot(self.params.wc.value))

            c = hf*cprev  + hi*hc
            h = ho*tanh(c)

            self.state.append([c, h])
            self.cache.append([x, hf, hi, ho, hc])

        out = softmax(h.dot(self.params.wy.value))

        return out

    def train(self, X, Y):
        #Y = one_hot(Y, 10)
        out = self.forward(X)
        entropy = cross_entropy(out, Y)
        self.backprop(X, out, Y)
        return entropy

    def backprop(self, X, out, Y):
        c , h = self.state[-1]
        gradY = out - Y
        self.params.wy.derivative = h.T.dot(gradY)
        dcnext = np.zeros_like(c)
        dhnext = np.zeros_like(h)
        for t in range(X.shape[0]):
            c , h = self.state[-t-1]
            tc = tanh(c)
            cprev, hprev = self.state[-t-2]
            x, hf, hi, ho, hc = self.cache[-t-1]
            dh = gradY.dot(self.params.wy.value.T)
            dc = dh*ho*dtanh(tc) + dcnext
            dho = dh*tc*dsigmoid(ho)
            dhf = dc*cprev*dsigmoid(hf)
            dhi = dc*hc*dsigmoid(hi)
            dhc = dc*hi*dtanh(hc)

            self.params.wf.derivative = x.T.dot(dhf) 
            self.params.wi.derivative = x.T.dot(dhi) 
            self.params.wo.derivative = x.T.dot(dho)
            self.params.wc.derivative = x.T.dot(dhc)

            if self.activation == "sdg":
                self.params.wf.sdg(self.lr)
                self.params.wi.sdg(self.lr)
                self.params.wc.sdg(self.lr)
                self.params.wo.sdg(self.lr)
                self.params.wy.sdg(self.lr)

            if self.activation == "adam":
                self.params.wf.adam(self.lr)
                self.params.wi.adam(self.lr)
                self.params.wc.adam(self.lr)
                self.params.wo.adam(self.lr)
                self.params.wy.adam(self.lr)                
