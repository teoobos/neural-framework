import numpy as np


def one_hot(x, dim):
    x[np.arange(dim), x] = 1
    return x
