import numpy as np


def squared(y1: np.array, y2: np.array):
    return np.square(y1 - y2)

def dsquared(y1: np.array, y2: np.array):
    return 2.0 * (y1 - y2)

def cross_entropy(out: np.array, label: np.array):
    return label * np.log(out + 1e-6)
