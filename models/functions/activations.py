import numpy as np


def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def dsigmoid(x):
    #return x * (1 - x)
    return sigmoid(x) * (1 - sigmoid(x))

def tanh(x):
    return np.tanh(x)

def dtanh(x):
    return 1 - np.square(tanh(x))

def relu(x):
    return np.maximum(0, x)

def drelu(x):
    return 1 if x > 0 else 0

def softmax(y):
    return np.exp(y) / np.sum(np.exp(y))
