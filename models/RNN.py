import numpy as np

from .functions.activations import dsigmoid, sigmoid
from .functions.loss import dsquared, squared
from .params.parameters import Params


class RNN:
    def __init__(self, input_dim, hidden_dim, output_dim, learning_rate=1e-6, optimizer="sdg"):
        self.input = input_dim
        self.hidden = hidden_dim
        self.output = output_dim
        self.lr = learning_rate
        self.optimizer = optimizer
       
        self.wi = Params('wi', np.random.randn(self.input, self.hidden))
        self.wh = Params('wh', np.random.randn(self.hidden, self.hidden))
        self.wo = Params('wo', np.random.randn(self.hidden, self.output))

        self.bi = Params('bi', np.zeros_like(self.wi))
        self.bh = Params('bh', np.zeros_like(self.wh))
        self.bo = Params('bo', np.zeros_like(self.wo))

        self.h = Params('h', np.zeros(self.hidden))
        self.c = Params('c', np.zeros(self.hidden))
       
    def forward(self, x):
        # ((Ix1.T IxH).T Hx1 + (Hx1.T HxH).T Hx1 ).T    (Hx1.T HxO).T  Ox1
        self.h.value = sigmoid(x.dot(self.wi.value) +
                               self.h.value.dot(self.wh.value))  # Hx1
        
        out = sigmoid(self.h.value.dot(self.wo.value))  # Ox1
        return out

    def backprop(self, X, out, Y):
        #Gradient
        gradY = dsquared(out, Y)
        self.wo.derivative = dsigmoid(self.h.value.T.dot(gradY))  # Hx1 Ox1 -> HxO
        self.h.derivative = dsigmoid(gradY.dot(self.wo.value.T))  # (Ox1.T HxO.T).T -> Hx1
        self.wi.derivative = X.T.dot(self.h.value)  # Ix1 Hx1-> IxH
        self.wh.derivative = self.h.derivative.T.dot(self.h.value)  # Hx1 Hx1-> HxH
        
        if self.optimizer == "sgd":
            
            self.wo.sdg(self.lr) 
            self.wi.sdg(self.lr)
            self.wh.sdg(self.lr)

        if self.optimizer == "adagrad":

            self.wo.adagrad(self.lr, 1e-8)   
            self.wi.adagrad(self.lr, 1e-8)
            self.wh.adagrad(self.lr, 1e-8) 

    def train(self, X, Y, epochs):
        for t in range(epochs):
            out = self.forward(X)
            delta = squared(out, Y)
            if t % 10 == 0:
                print(t, delta.sum())
            self.backprop(X, out, Y)
            if t % 10 == 0:
                print(t, delta.sum())
        return out, delta.sum()
