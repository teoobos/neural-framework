import numpy as np

from .functions.activations import drelu, relu
from .functions.loss import dsquared, squared
from .params.parameters import Params


class ReLU:
    def __init__(self, input_dim, hidden_dim, output_dim, learning_rate=0.001, optimizer="adagrad"):
        self.input = input_dim
        self.hidden = hidden_dim
        self.output = output_dim
        self.lr = learning_rate
        self.wi = Params('wi', np.random.randn(self.input, self.hidden))
        self.wh = Params('wh', np.random.randn(self.hidden, self.output))
        self.optimizer = optimizer

    def forward(self, X):
        h = relu(X.dot(self.wi.value))
        output = h.dot(self.wh.value)
        return output

    def backprop(self, X, out, Y):
        gradY = dsquared(out, Y)
        self.wh.derivative = relu(X.dot(self.wi.value)).T.dot(gradY)
        grad_h = relu(gradY.dot(self.wh.value.T))
        self.wi.derivative = X.T.dot(grad_h)

        if self.optimizer == "sdg":
            self.wi.sdg(self.lr)
            self.wh.sdg(self.lr)

        if self.optimizer == "adagrad":
            self.wi.adagrad(self.lr, eps=1e-8)
            self.wh.adagrad(self.lr, eps=1e-8)

    def train(self, X, Y, epochs):
        for t in range(epochs):
            print(t)
            out = self.forward(X)
            delta = squared(out, Y)
            self.backprop(X, out, Y)
        return out, delta.sum()
