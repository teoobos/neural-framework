import numpy as np

from .parameters import Params


class LSTMParams(Params):
    def __init__(self, input_dim, hidden_dim, output_dim, standard_deviation=0.1):
        self.wsd = standard_deviation
        self.input = input_dim
        self.hidden = hidden_dim
        self.output = output_dim
        self.recurrent = input_dim + hidden_dim

        self.wf = Params('wf', np.random.randn(
            self.hidden, self.recurrent) * self.wsd + 0.5)
        self.wi = Params('wi', np.random.randn(
            self.hidden, self.recurrent) * self.wsd + 0.5)
        self.wc = Params('wc', np.random.randn(
            self.hidden, self.recurrent) * self.wsd + 0.5)
        self.wo = Params('wo', np.random.randn(
            self.hidden, self.recurrent) * self.wsd + 0.5)
        self.wv = Params('wv', np.random.randn(
            self.input, self.hidden) * self.wsd + 0.5)
        self.wy = Params('wy', np.random.rand(
            self.hidden, self.output) * self.wsd + 0.5)

        #self.bf=Params('bf', np.zeros(self.hidden, 1))
        #self.bi=Params('bi', np.zeros(self.hidden, 1))
        #self.bc=Params('bc', np.zeros(self.hidden, 1))
        #self.bo=Params('bo', np.zeros(self.hidden, 1))
        #self.bv=Params('bv', np.zeros(self.input, 1))
        #self.by=Params('by', np.zeros(self.output, 1))

    #def get_params(self):
    #    return [self.wf, self.bf, self.wi, self.bi, self.wc, self.bc, self.wo, self.bo, self.wv, self.bv, self.wy, self.by]
