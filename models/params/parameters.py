import numpy as np


class Params:
    def __init__(self, name, value):
        self.name = name
        self.value = value
        self.derivative = np.zeros_like(value)
        self.momentum = np.zeros_like(value)

    def sdg(self, rate):
        self.value -= rate * self.derivative

    def adagrad(self, rate, eps):
        self.momentum = np.square(self.derivative)
        self.value -= rate * self.derivative / np.sqrt(self.momentum + eps)
