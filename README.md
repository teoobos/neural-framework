# Neural Framework

## Requirements

You only need:

* Numpy
* Pandas

## Features

The currenlty supported Neural Networks are:

* 2 Layer ReLU
* RNN 

Actualy the Datetime format is not supperted

## Usage
### ReLU
``` python
from readers.CsvData import CsvData
from models.RNN import RNN
from models.ReLU import ReLU

reader = CsvData()

X, Y = reader.splitter('data.csv', ["p", "rh", "wv"], ["T"])

for column in X.columns:
    X[column] = X[column] / X[column].max()

for column in Y.columns:
    Y[column] = Y[column] / Y[column].max()

model = ReLU(3, 10, 1, 1e-6)
out, loss = model.train(X.values, Y.values, 400)
print(out, loss)
```

### RNN
```python
from readers.CsvData import CsvData
from models.RNN import RNN
from models.ReLU import ReLU

reader = CsvData()

X, Y = reader.splitter('data.csv', ["p", "rh", "wv"], ["T"])

for column in X.columns:
    X[column] = X[column] / X[column].max()

for column in Y.columns:
    Y[column] = Y[column] / Y[column].max()

model = RNN(3, 10, 1, 1e-6)
out, loss = model.train(X.values, Y.values, 400)
print(out, loss)
```
